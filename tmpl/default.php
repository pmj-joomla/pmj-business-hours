<?php
/**
 * @package Module PMJ Business Hours for Joomla! 3.8
 * @author PMJ
 * @copyright (C) 2018- PMJ
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die;
?>
<?php if ($params->get('statusDisplay')) { ?>
	<?php if ($params->get('statusPosition') === 'top') { ?>
	<p class="<?php echo $params->get('statusContainerClass'); ?><?php echo $status === JText::_('MOD_PMJ_BUSINESS_HOURS_OPEN') ? ' '.$params->get('statusOpenClass') : ' '.$params->get('statusClosedClass'); ?>">
		<?php echo $params->get('statusUppercase') ? mb_strtoupper(sprintf(JText::_('MOD_PMJ_BUSINESS_HOURS_STATUS_MESSAGE'),$status)) : sprintf(JText::_('MOD_PMJ_BUSINESS_HOURS_STATUS_MESSAGE'),$status); ?>
	</p>
	<?php } ?>
<?php } ?>
<?php if ($items) { ?>
<table class="table table-condensed table-borderless table-sm" style="border:0;">
	<thead class="pmj-sr-only">
		<tr>
			<th><?php echo JText::_('MOD_PMJ_BUSINESS_HOURS_DAY'); ?></th>
			<th><?php echo JText::_('MOD_PMJ_BUSINESS_HOURS_TIME'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($items as $day => $time) { ?>
		<tr>
			<td><?php echo $day; ?></td>
			<td style="white-space: nowrap;"><?php echo $time; ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php } ?>
<?php if ($params->get('statusDisplay')) { ?>
	<?php if ($params->get('statusPosition') === 'bottom') { ?>
	<p class="<?php echo $params->get('statusContainerClass'); ?><?php echo $status === JText::_('MOD_PMJ_BUSINESS_HOURS_OPEN') ? ' '.$params->get('statusOpenClass') : ' '.$params->get('statusClosedClass'); ?>">
		<?php echo $params->get('statusUppercase') ? mb_strtoupper(sprintf(JText::_('MOD_PMJ_BUSINESS_HOURS_STATUS_MESSAGE'),$status)) : sprintf(JText::_('MOD_PMJ_BUSINESS_HOURS_STATUS_MESSAGE'),$status); ?>
	</p>
	<?php } ?>
<?php } ?>
<?php if ($offDays && $params->get('offDaysDisplay')) { ?>
<h4><?php echo JText::_('MOD_PMJ_BUSINESS_HOURS_OFFDAYS'); ?></h4>
<table class="table table-condensed table-borderless table-sm" style="border:0;">
	<thead class="pmj-sr-only">
		<tr>
			<th><?php echo JText::_('MOD_PMJ_BUSINESS_HOURS_DESCRIPTION'); ?></th>
			<th><?php echo JText::_('MOD_PMJ_BUSINESS_HOURS_DATES'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($offDays as $description => $exception) { ?>
		<tr>
			<?php if ($description !== 'none') : ?>
			<td><?php echo $description; ?></td>
			<?php endif ; ?>
			<td <?php echo $description !== 'none' ? 'colspan="2"': ''; ?>><?php echo $exception; ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php } ?>