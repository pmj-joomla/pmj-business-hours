## Changelog

### 1.1

#### 1.1.4 [2018-10-30]

* Fixed a small bug in offDaysFormat parameter

#### 1.1.3 [2018-10-29]

* Forgot to change version numbers, d'oh

#### 1.1.2 [2018-10-29]

* Some corrections for readme

#### 1.1.1 [2018-10-29]

* Version
* Change Readme to reflect changes
* Change exceptions parameters and variables to day off
* Change time parameter variables
* Forgot to remove hardcoded class in status
* Change status parameter variables

#### 1.1.0 [2018-10-27]

* Skip minor version because of too many changes
* Add Exceptions Display
* Typos in Readme
* Remove .well and add individual class(es), clean up settings
* Adding TOC to Readme
* Fixed updates.xml -> update.xml
* Updated Readme

---
### 1.0

#### 1.0.0 [2018-10-20]

* Add Update Server
* Switch hours and reason in exceptions
* Initial Commit