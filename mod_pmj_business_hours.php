<?php
/**
 * @package Module PMJ Business Hours for Joomla! 3.8
 * @author PMJ
 * @copyright (C) 2018- PMJ
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die;

// include helper
JLoader::register('ModPmjBusinessHoursHelper', __DIR__ . '/helper.php');

// add a small css declaration for screen readers
$doc	= JFactory::getDocument();
$doc->addStyleDeclaration('.pmj-sr-only{position:absolute;width:1px;height:1px;padding:0;overflow:hidden;clip:rect(0,0,0,0);white-space:nowrap;border:0}');

// set days
$days	= array(
	'monday',
	'tuesday',
	'wednesday',
	'thursday',
	'friday',
	'saturday',
	'sunday',
);

$items	= ModPmjBusinessHoursHelper::compileItems($params,$days);
if ($params->get('offDaysDisplay'))
{
	$offDays	= ModPmjBusinessHoursHelper::compileOffDays($params);
}
$status	= ModPmjBusinessHoursHelper::status($params, $days);
$layout	= $params->get('layout', 'default');

require JModuleHelper::getLayoutPath('mod_pmj_business_hours', $layout);
