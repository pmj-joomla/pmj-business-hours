# PMJ Business Hours

PMJ Business Hours is a very simple Joomla module to - you might have guessed it - display business hours.  
It works in a **24h** setting.

It has been developed and tested using Joomla 3.8, so this will officially be considered the minimum version although it should work with all Joomla 3+ versions.

## Table of contents

* [Downloads](#downloads)
* [Features](#features)
* [How to use it](#how-to-use-it)
* [Override](#override)
* [Languages](#languages)

## Downloads

**[Current Stable Version (1.1.3)](https://gitlab.com/pmj-joomla/pmj-business-hours/-/archive/1.1.4/pmj-business-hours-1.1.4.zip)**

**[Developement Version](https://gitlab.com/pmj-joomla/pmj-business-hours/-/archive/dev/pmj-business-hours-dev.zip)**

## Features

* Display business hours inline or in blocks
* Display business hours daily or combined if the times of more than two consecutive days are the same
* Display or hide closed days
* Display or hide current status - open or closed above or below the times
* Set CSS class(es) for status container
* Set exceptions like holidays  
They will also be used to calculate the current status  
You can even set a reason
* Display or hide exceptions
* Set delimiter for 'until' and times

## How to use it

First of all, you need to **set the global timezone for your Joomla** installation!  
If you don't do it, the status will be calculated for the server time, and your business may be considered opened or closed when it's not.

### Settings

Hopefully, the module settings are self-explanatory enough.  
Of course, you can always set several options and check how they work in the frontend.

### Set times

The times for the days are very simple to set. Just set them like you would anyway.  
`8:15-12:00` or `9:00-17:00` or `8:00-12:00,13:00-17:00`  
If you leave the field empty, it will be considered closed.

### Set off days

The off days might be a little more complicated to handle.  
They are set with the following syntax:  
`MM-DD,(description),(hours);` for single days  
or  
`MM-DD=MM-DD,(description),(hours);` for consecutive days.  
Hours and description are optional parameters and are meant to give a more accurate description for your customers.  
Hours are meant to define the beginning time of the first day set until the end time of the last day set.  
If you you don't want to give a description but still want to use the hours setting, you would have to set it like this:  
`12-24=12-27,,12:00-8:00;`  
(which means your business will be considered closed from december 24th 12:00 until december 27th 8:00)

## Override

### Variables

There are three variables which can be used for overrides in the template (default.php)

* `(array) $items`  
this is the main array which holds days and times for business hours  
it has the following setup:  
`$item[translated name of day or consecutive days] => time(s)`
* `(array) $offDays`  
this array holds the off days combined by description  
it has the following setup:  
`$offDays[description if set, else none (string) will be used] => off day(s)`
* `(string) $status`  
holds the translated status message and, if set, the description for off days in []

### Parameters

A list of all paramters  and their return values which can be set in the backend and how they are used by default.  
Values in bold are default settings.  

#### Status

* `statusDisplay` (**1**/0)  
display the status message
* `statusUppercase` (**1**/0)  
transform the status message to uppercase
* `statusPosition` (**top**/bottom)  
where to display the status message
* `statusOpenClass` (text - **text-success**)  
set individual class(es) for status 'open' text
* `statusClosedClass` (text - **text-error**)  
set individual class(es) for status 'closed' text
* `statusContainerClass` (text)  
set individual class(es) for the status container

#### Time

* `timeDisplayStyle` (**block**/inline)  
how the days and times are displayed  
inline puts them into one line while block adds a line break after each element
* `timeDisplayClosed` (1/**0**)  
display closed days
* `timeCombine` (**1**/0)  
combine days with the same time(s) into one listing entry
* `delimiterUntil` (text - **&ndash;**)  
delimiter for 'until'
* `delimiterTime` (text - **:**)  
delimiter for time(s)

#### Off days

* `offDaysDisplay` (1/**0**)  
display off days
* `offDaysDisplayStyle` (block/**inline**)  
how off days are displayed  
inline puts them into one line while block adds a line break after each element
* `offDaysDescription` (1/**0**)  
show off days descriptions if set
* `offDaysHours` (1/**0**)  
display off days hours if set
* `offDaysFormat` (**j. F**/d. F/j. M./d. M.)  
how to format off days dates

## Languages

PMJ Business Hours currently offers two languages:
* english (en-GB)
* german (de-DE)

If anyone is willing to translate the module into other languages you would be very welcomed!