<?php
/**
 * @package Module PMJ Business Hours for Joomla! 3.8
 * @author PMJ
 * @copyright (C) 2018- PMJ
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die;

class ModPmjBusinessHoursHelper
{
	public static function compileItems($params, $days)
	{
		$items	= array();
		foreach ($days as $day)
		{
			// streamline entries
			$entry	= str_replace(
				// replace dash with a longer dash
				'-',' '.$params->get('delimiterUntil').' ',str_replace(
					// add space after ,
					',',', ',str_replace(
						// remove spaces and new lines
						array(' ','  ','   ','    ',PHP_EOL),'',str_replace(
							// replace new lines with ,
							PHP_EOL,',',$params->get($day)
						)
					)
				)
			);
			// pretify times (leading zeros)
			$entry	= !empty($entry) ? self::pretifyTimes($entry,$params) : '';
			
			// replace , with a new line when in block mode
			if ($params->get('timeDisplayStyle') == 'block')
			{
				$entry	= str_replace(', ','<br />',$entry);
			}
			// check if closed days should be displayed
			if ( !( !$params->get('timeDisplayClosed') && empty($entry) ) )
			{
				$items[JText::_($day)]	= !empty($entry) ? $entry : JText::_('MOD_PMJ_BUSINESS_HOURS_CLOSED');
			}
		}
		// do we need to combine?
		if ($params->get('timeCombine'))
		{
			$items	= self::combineItems($items,$params,$days);
		}
		return $items;
	}
	
	public static function compileOffDays($params)
	{
		// if there are no offDays set bail out
		if (!$params->get('offDays'))
		{
			return false;
		}
		$item	= array();
		$offDays	= self::calcOffDays($params->get('offDays'));
		foreach ($offDays as $offDay)
		{
			// time
			$time	= explode('/',$offDay['time']);
			$start	= JFactory::getDate($time[0])->format($params->get('offDaysFormat'));
			$end	= JFactory::getDate($time[1])->format($params->get('offDaysFormat'));
			
			$description	= $offDay['description'] && $params->get('offDaysDescription') ? $offDay['description'] : 'none';
			
			// check if it's a period or a single entry
			if ($start	== $end)
			{
				$time	= $start;
				if ($offDay['hours'] && $params->get('offDaysHours'))
				{
					$time	= $time.' '.str_replace(', ',' '.$params->get('delimiterUntil').' ',self::pretifyTimes(str_replace('-',', ',$offDay['hours']),$params));
				}
			}
			else
			{
				$time	= $start.' '.$params->get('delimiterUntil').' '.$end;
				if ($offDay['hours'] && $params->get('offDaysHours'))
				{
					$hours	= self::pretifyTimes(str_replace('-',', ',$offDay['hours']),$params);
					$hours	= explode(', ',$hours);
					$time	= $start.' '.$hours[0].' '.$params->get('delimiterUntil').' '.$end.' '.$hours[1];
				}
			}
			
			// display style
			if ($params->get('offDaysDisplayStyle') === 'block')
			{
				$item[$description]	= $item[$description] ? $item[$description]."<br />".$time : $time;
			}
			else
			{
				$item[$description]	= $item[$description] ? $item[$description].", ".$time : $time;
			}
		}
		ksort($item);
		return $item;
	}
	
	public static function status($params, $days)
	{
		$timezone	= JFactory::getConfig()->get('offset');
		$now	= new JDate('now',$timezone);
		$now	= $now->toUnix();
		
		// check offDays first and bail out
		if ($params->get('offDays'))
		{
			//get calculated offDays
			$offDays	= self::calcOffDays($params->get('offDays'));
			foreach ($offDays as $offDay)
			{
				$time	= explode('/',$offDay['time']);
				$start	= $time[0];
				$start	= new JDate($time[0],$timezone);
				$start	= $start->toUnix();
				$end	= $time[1];
				$end	= new JDate($time[1],$timezone);
				$end	= $end->toUnix();
			
				if ($now > $start && $now < $end)
				{
					// check if there's a description
					if ($offDay['description'])
					{
						$description	= ' ['.$offDay['description'].']';
					}
					else
					{
						$description	= '';
					}
					return	JText::_('MOD_PMJ_BUSINESS_HOURS_CLOSED').$description;
				}
			}
		}
		
		// get current day
		$today	= strtolower(date('l',$now));
		// if today has no time set, return closed and bail out
		if (!$params->get($today))
		{
			return JText::_('MOD_PMJ_BUSINESS_HOURS_CLOSED');
		}
		
		$today_date	= JFactory::getDate($now)->format('Y-n-d');
		// streamline times
		$times	= str_replace(
			// replace dash with a longer dash
			'-',' '.$params->get('delimiterUntil').' ',str_replace(
				// add space after ,
				',',', ',str_replace(
					// remove spaces and new lines
					array(' ','  ','   ','    ',PHP_EOL),'',str_replace(
						// replace new lines with ,
						PHP_EOL,',',$params->get($today)
					)
				)
			)
		);
		// pretify times (leading zeros)
		$times	= !empty($times) ? self::pretifyTimes($times,$params) : '';
		// create array of times
		$times	= explode(', ',$times);
		
		foreach ($times as $time)
		{
			if (strpos($time,'&ndash;') !== false)
			{
				$time	= explode(' &ndash; ',$time);
				$start	= $today_date.' '.$time[0];
				$end	= $today_date.' '.$time[1];
				
				$start	= new JDate($start,$timezone);
				$start	= $start->toUnix();
				$end	= new JDate($end,$timezone);
				$end	= $end->toUnix();
				
				if ($now > $start && $now < $end)
				{
					return	JText::_('MOD_PMJ_BUSINESS_HOURS_OPEN');
				}
			}
		}
		
		// default 'closed'
		return JText::_('MOD_PMJ_BUSINESS_HOURS_CLOSED');
	}
	
	// calculate offDays
	private static function calcOffDays($offDays)
	{
		// set now
		$timezone	= JFactory::getConfig()->get('offset');
		$now	= new JDate('now',$timezone);
		$now	= $now->toUnix();
		// set year
		$year	= JFactory::getDate($now)->format('Y');
		// set variables for periods check
		$current_month	= JFactory::getDate($now)->format('m');
		$current_day	= JFactory::getDate($now)->format('j');
		$current_hour	= JFactory::getDate($now)->format('H');
		$current_minute	= JFactory::getDate($now)->format('i');
		
		$offDays	= str_replace("\r\n",'',$offDays);
		// check last char
		if (substr($offDays,-1) === ';')
		{
			$offDays	= substr_replace($offDays ,"",-1);
		}
		// offDays to array
		$offDays	= explode(';',$offDays);
		// sort offDays
		sort($offDays,SORT_NATURAL);
		$calcs	= array();
		foreach ($offDays as $offDay)
		{
			$calc	= array();
			// split day(s), description, hours
			$offDay	= explode(',',$offDay);
			list($day,$description,$hours) = $offDay;
			
			// set year
			$start_year	= $year;
			$end_year	= $year;
					
			// compile day
			if ($day)
			{
				// check and set hours
				if (strpos($hours,'-') !== false)
				{
					$calc['hours']	= $hours;
					$hours	= explode('-',$hours);
					$start_time	= $hours[0];
					$end_time	= $hours[1];
				}
				else
				{
					$start_time	= '00:00';
					$end_time	= '23:59';
				}
				// check if it's a period
				if (strpos($day,'=') !== false)
				{
					$days	= explode('=',$day);
					// start
					$start	= explode('-',$days[0]);
					list($start_month,$start_day) = $start;
					// end
					$end	= explode('-',$days[1]);
					list($end_month,$end_day) = $end;
					// check if the period passes the year
					if ($end_month < $start_month && $current_month >= $end_month && $current_day >= $end_day && $current_hour >= explode(':',$end_time)[0] && $current_minute >= explode(':',$end_time)[1])
					{
						$end_year++;
					}
					elseif ($end_month < $start_month)
					{
						$start_year--;
					}
				}
				else
				{
					$day	= explode('-',$day);
					list($month,$day) = $day;
					$start_month	= $end_month	= $month;
					$start_day	= $end_day	= $day;
				}
				
				// compile timestamps
				$start	= $start_year.'-'.$start_month.'-'.$start_day.' '.$start_time;
				$end	= $end_year.'-'.$end_month.'-'.$end_day.' '.$end_time;
				$calc['time']	= $start.'/'.$end;
				
				// compile description
				if ($description)
				{
					$calc['description']	= $description;
				}
				
				$calcs[]	= $calc;
			}
		}
		
		return $calcs;
	}
	
	// pretify times (leading zeros)
	private static function pretifyTimes($entry,$params)
	{
		$pretified	= array();
		// split times into single items
		// split times
		$times	= explode(', ',$entry);
		foreach ($times as $time)
		{
			// split time
			$items	= explode(' '.$params->get('delimiterUntil').' ',$time);
			$pretified_time	= array();
			foreach ($items as $item)
			{
				// split items
				$new	= explode(':',$item);
				$pretified_time[]	= sprintf("%02d", $new[0]).$params->get('delimiterTime').sprintf("%02d", $new[1]);
			}
			$pretified[]	= implode(' '.$params->get('delimiterUntil').' ',$pretified_time);
		}
		$pretified	= implode(', ',$pretified);
		return $pretified;
	}
	
	// combine days with the same times
	private static function combineItems($items,$params,$days)
	{
		// flip items so the day becomes the value
		$flip = array_flip($items);
		// combine days with the same time
		foreach ($flip as $time => $day)
		{
			// delimiter based upon block or inline display
			$delimiter	= $params->get('timeDisplayStyle') == 'block' ? '<br />' : ', ';
			$flip[$time] = implode($delimiter, array_keys($items, $time));
			// check for consecutive days (only for more than 3 days)
			$time_days	= array_keys($items, $time);
			
			if (count($time_days) > 2)
			{
				$consecutive	= true;
				foreach ($days as $dindex => $d)
				{
					if ($time_days[0] === JText::_($d))
					{
						$start_day_index	= $dindex;
						break;
					}
				}
				
				foreach ($time_days as $i => $td)
				{
					if ($td	!== JText::_($days[$start_day_index]))
					{
						$consecutive	= false;
					}
					$start_day_index++;
				}
				
				if ($consecutive)
				{
					$flip[$time]	= $time_days[0].' '.$params->get('delimiterUntil').' '.end($time_days);
				}
			}
		}
		// flip it back so the time becomes the value again
		$items	= array_flip($flip);
		// move closed item to bottom of array
		$bottom_key	= array_search(JText::_('MOD_PMJ_BUSINESS_HOURS_CLOSED'),$items);
		if ($bottom_key)
		{
			unset($items[$bottom_key]);
			$items[$bottom_key]	= JText::_('MOD_PMJ_BUSINESS_HOURS_CLOSED');
		}
		
		return $items;
	}
}